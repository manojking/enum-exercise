package bcas.ap.enums;

public class RentalProperty {
	private long []propertyNumbers;
	private PropertyEnum []types;
	private short[]bedroom;
	private float[]bathroom;
	private double[]monthlyRent;
	
	public RentalProperty () {
		propertyNumbers = new long [] {1234,5364,2363,2236,6783};
		types = new PropertyEnum [] {PropertyEnum.APPARTMENT, PropertyEnum.DOUBLEBEDROOM, PropertyEnum.SINGLEFAMILY, PropertyEnum.ONEBEDROOM};
		bedroom= new short[] {2,1,3,2,1};
		bathroom= new float[] {3.50F, 2.10F, 1.00F, 2.40F};
		monthlyRent= new double[] {200.50D, 1885.00D, 1175.50D, 500.50D};
		
		public void showListing() {
			System.out.println("properties listing");
			System.out.println("---------------------------");
			for (int i=0; i<8; i++) {
				System.out.println(propertyNumbers[i]);
				System.out.println(types[i]);
				System.out.println(bedroom[i]);
				System.out.println(bathroom[i]);
				System.out.println(monthlyRent[i]);
			
			}
		}
		
		
	}
	

}
